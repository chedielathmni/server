const { ApolloServer } = require('apollo-server');

const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const ApolloConfig = require('./apolloConfig');


const config = {
  context: ApolloConfig.context,
  typeDefs,
  resolvers,
  dataSources: ApolloConfig.dataSources
}

const server = new ApolloServer(config);

module.exports = server;