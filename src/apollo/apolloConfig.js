const isEmail = require('isemail');

const { createStore } = require('../utils/utils');
const store = createStore();

const LaunchAPI = require('../datasources/launch');
const UserAPI = require('../datasources/user');

module.exports = class ApolloConfig {

  static dataSources = () => {
    return {
      launchAPI: new LaunchAPI,
      userAPI: new UserAPI({ store })
    }
  }

  static context = async({ req }) => {
    const auth = req.headers && req.headers.authorization || '';
    const email = Buffer.from(auth, 'base64').toString('ascii');
    if (!isEmail.validate(email)) return { user: null };
    // find a user by their email
    const users = await store.users.findOrCreate({ where: { email } });
    const user = users && users[0] || null;
    return { user: { ...user.dataValues } };
  }
}
