require('dotenv').config();

// Apollo Config
const server = require('./apollo');

server.listen().then(({ port }) => {
  console.log(`
    Server is running!
    Listening on port ${port}
  `);
});